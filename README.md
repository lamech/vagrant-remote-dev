# Dan's Vagrant setup for remote dev

Features:

* Vagrant integration with Digital Ocean.
* Create/destroy droplet on the back end via `vagrant up` and `vagrant destroy`, respectively.
* Rsyncs local dir to /vagrant on the Droplet.
* Provisions with Ansible (see vagrantup.yaml):
	* Secures root account.
	* Sets up a non-root user with sudo.
	* Installs prerequisites (Docker, make, net-tools etc.).
	* Copies over my .bashrc and other dot-files.
	* Installs DDEV as the non-root user.

# Vagrant

On Linux, probably use the deb or a download. The Homebrew command might not work (it doesn't in April 2023).

You will also need the `vagrant-digitalocean` plugin:

```
vagrant plugin install vagrant-digitalocean
```

# DigitalOcean API Integration

You'll need a [DigitalOcean API key](https://docs.digitalocean.com/reference/api/create-personal-access-token/), preferably secured by something like [pass](https://www.passwordstore.org/).

For example, `$DO_DROPLET_AUTH_HELPER` is set as follows in my `.zshrc`:

```
export DO_DROPLET_AUTH_HELPER='pass path/to/my/do-api-key'
```

TO DO:

* Add [Drumkit](https://gitlab.com/consensus.enterprises/drumkit)/add template for this to Drumkit.
